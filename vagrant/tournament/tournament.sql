-- Table definitions for the tournament project.
--
-- Put your SQL 'create table' statements in this file; also 'create view'
-- statements if you choose to use it.
--
-- You can write comments in this file by starting them with two dashes, like
-- these lines here.

CREATE DATABASE tournament;
\c tournament;

CREATE TABLE players(
  player_id serial,
  player_name text
);

CREATE TABLE matches(
  match_id serial,
  p1_id int,
  p2_id int,
  winner_id int
);

-- wins per player ordered by player_id
CREATE VIEW wins AS
  SELECT players.player_id, wins.wins
  FROM players LEFT JOIN (
    SELECT winner_id AS player_id, COUNT(*) AS wins
    FROM matches
    GROUP BY player_id
    ORDER BY player_id
  ) AS wins ON players.player_id=wins.player_id;

-- count total matches
CREATE VIEW total_matches AS
  SELECT player_id, count(*) AS match_count
  FROM (
    SELECT p1_id AS player_id
    FROM matches
    UNION ALL
    SELECT p2_id
    FROM matches
  ) AS matches
  GROUP BY player_id
  ORDER BY player_id;

--standings view
--player_id, player_name, wins, matches
CREATE VIEW standings AS
  SELECT players.player_id, players.player_name, 
    COALESCE(wins, 0) AS wins, COALESCE(match_count, 0) AS matches
  FROM players LEFT JOIN (
    SELECT total_matches.player_id, wins, match_count
    FROM wins, total_matches
    WHERE wins.player_id=total_matches.player_id
  ) AS matches
  ON players.player_id=matches.player_id
  ORDER BY wins DESC;

-- A view for standings with row numbers to make easier to get pairs
CREATE VIEW numbered_standings AS
  SELECT standings.player_id, player_name, wins, matches, row_number
  FROM standings, (
    SELECT player_id, ROW_NUMBER() OVER (
      ORDER BY wins DESC)
    FROM standings) AS r
  WHERE standings.player_id=r.player_id
  ORDER BY row_number;

