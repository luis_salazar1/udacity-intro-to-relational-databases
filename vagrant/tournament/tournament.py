#!/usr/bin/env python
# 
# tournament.py -- implementation of a Swiss-system tournament
#

import psycopg2


def connect(do, conn=None, cur=None):
    """Connect to the PostgreSQL database.  Returns a database connection."""
    # connect to db & return connection and cursor
    if do == 'connect':
        conn = psycopg2.connect("dbname=tournament")
        cur = conn.cursor()
        return conn, cur

    # commit changes to db
    elif do == 'commit':
        conn.commit()

    # close cursor and connnection to db
    elif do == 'close':
        cur.close()
        conn.close()


def deleteMatches():
    """Remove all the match records from the database."""

    conn, cur = connect('connect')
    cur.execute('DELETE FROM matches')
    connect('commit', conn)
    connect('close', conn, cur)


def deletePlayers():
    """Remove all the player records from the database."""
    
    conn, cur = connect('connect')
    cur.execute('DELETE FROM players')
    connect('commit', conn)
    connect('close', conn, cur)


def countPlayers():
    """Returns the number of players currently registered."""

    conn, cur = connect('connect')

    cur.execute('SELECT COUNT(*) FROM players')

    playerCount = int(cur.fetchone()[0])
    connect('close',conn, cur)

    return playerCount

    
def registerPlayer(name):
    """Adds a player to the tournament database.
  
    The database assigns a unique serial id number for the player.  (This
    should be handled by your SQL database schema, not in your Python code.)
  
    Args:
      name: the player's full name (need not be unique).
    """
 
    conn, cur = connect('connect')

    # insert players
    cur.execute('''INSERT INTO players (player_name)
        VALUES (%s)''', (name,))

    # commit & close
    connect('commit', conn)
    connect('close',conn, cur)


def playerStandings():
    """Returns a list of the players and their win records, sorted by
    wins.

    The first entry in the list should be the player in first place,
    or a player
    tied for first place if there is currently a tie.

    Returns:
      A list of tuples, each of which contains (id, name, wins, matches):
        id: the player's unique id (assigned by the database)
        name: the player's full name (as registered)
        wins: the number of matches the player has won
        matches: the number of matches the player has played
    """
    
    conn, cur = connect('connect')
    cur.execute('SELECT * FROM standings')
    standings = cur.fetchall()
    connect('close', conn, cur)

    return standings



def reportMatch(winner, loser):
    """Records the outcome of a single match between two players.

    Args:
      winner:  the id number of the player who won
      loser:  the id number of the player who lost
    """

    conn, cur = connect('connect')

    cur.execute('''INSERT INTO matches (p1_id, p2_id, winner_id)
        VALUES (%s, %s, %s)''', (winner, loser, winner))

    connect('commit', conn)
    connect('close', conn, cur)

 
def swissPairings():
    """Returns a list of pairs of players for the next round of a match.
  
    Assuming that there are an even number of players registered, each player
    appears exactly once in the pairings.  Each player is paired with another
    player with an equal or nearly-equal win record, that is, a player adjacent
    to him or her in the standings.
  
    Returns:
      A list of tuples, each of which contains (id1, name1, id2, name2)
        id1: the first player's unique id
        name1: the first player's name
        id2: the second player's unique id
        name2: the second player's name
    """

    conn, cur = connect('connect')
    cur.execute('''
        SELECT p1.player_id, p1.player_name, p2.player_id, p2.player_name
          FROM (
            SELECT *, ROW_NUMBER() OVER (ORDER BY row_number) AS rn
            FROM numbered_standings
            WHERE row_number % 2 = 1) AS p1, ( 
            SELECT *, ROW_NUMBER() OVER (ORDER BY row_number) AS rn
            FROM numbered_standings
            WHERE row_number % 2 = 0) AS p2
         WHERE p1.rn=p2.rn;
    ''')
  
    pairings = cur.fetchall()
    connect('close', conn, cur)
    return pairings
